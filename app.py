import gdown
import os
MODEL_LINK = "https://drive.google.com/file/d/1XMdyxlKg7iliN6ekJVn9v4o6HJCJ-ASb/view?usp=drive_link"
MODEL_PATH = "model.pt"

if not os.path.exists(MODEL_PATH):
    print("Downloading model . . . ")
    gdown.download(MODEL_LINK,MODEL_PATH,fuzzy=True)

import torch
from S1_YoloTimber import YoloTimber
model:YoloTimber = torch.load(MODEL_PATH)
model.image_size = (320,320)

labels = ["Aspidosperma polyneuron", "Araucaria angustifolia", "Tabebuia sp.", "Cordia goeldiana", "Cordia sp.", "Hura crepitans", "Acrocarpus fraxinifolius", "Hymenaea sp.", "Peltogyne sp.", "Hymenolobium petraeum", "Myroxylon balsamum", "Dipteryx sp.", "Machaerium sp.", "Bowdichia sp.", "Mimosa scabrella", "Cedrelinga catenaeformis", "Goupia glabra", "Ocotea porosa", "Mezilaurus itauba", "Laurus nobilis", "Bertholethia excelsa", "Cariniana estrellensis", "Couratari sp.", "Carapa guianensis", "Cedrela fissilis", "Melia azedarach", "Swietenia macrophylla", "Brosimum paraense", "Bagassa guianensis", "Virola surinamensis", "Eucalyptus sp.", "Pinus sp.", "Podocarpus lambertii", "Grevilea robusta", "Balfourodendron riedelianum", "Euxylophora paraensis", "Micropholis venulosa", "Pouteria pachycarpa", "Manilkara huberi", "Erisma uncinatum", "Vochysia sp."]

import gradio as gr
import cv2

def classify(image):
    image = cv2.cvtColor(image, cv2. COLOR_RGB2BGR)
    with torch.no_grad():
        out = model.predict_large_image(image).item()
        out = labels[out]
    return out

demo = gr.Interface(fn=classify, 
                    inputs="image", 
                    outputs="text")

demo.launch()